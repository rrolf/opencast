/**
 * Licensed to The Apereo Foundation under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 *
 * The Apereo Foundation licenses this file to you under the Educational
 * Community License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License
 * at:
 *
 *   http://opensource.org/licenses/ecl2.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package org.opencastproject.adminui.endpoint;

import static com.entwinemedia.fn.data.json.Jsons.f;
import static com.entwinemedia.fn.data.json.Jsons.j;
import static com.entwinemedia.fn.data.json.Jsons.v;
import static com.entwinemedia.fn.data.json.Jsons.vN;
import static org.opencastproject.util.doc.rest.RestParameter.Type.STRING;

import org.opencastproject.index.service.util.RestUtils;
import org.opencastproject.matterhorn.search.SearchQuery;
import org.opencastproject.matterhorn.search.SortCriterion;
import org.opencastproject.serviceregistry.api.ServiceRegistry;
import org.opencastproject.serviceregistry.api.ServiceState;
import org.opencastproject.serviceregistry.api.ServiceStatistics;
import org.opencastproject.util.SmartIterator;
import org.opencastproject.util.data.Option;
import org.opencastproject.util.doc.rest.RestParameter;
import org.opencastproject.util.doc.rest.RestQuery;
import org.opencastproject.util.doc.rest.RestResponse;
import org.opencastproject.util.doc.rest.RestService;

import com.entwinemedia.fn.data.json.JValue;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@RestService(name = "ServicesProxyService", title = "UI Services",
        notes = "These Endpoints deliver informations about the services required for the UI.",
        abstractText = "This service provides the services data for the UI.")
public class ServicesEndpoint {
  private static final Logger logger = LoggerFactory.getLogger(ServicesEndpoint.class);
  private ServiceRegistry serviceRegistry;

  @GET
  @Path("services.json")
  @Produces(MediaType.APPLICATION_JSON)
  @RestQuery(description = "Returns the list of services", name = "services", restParameters = {
          @RestParameter(name = "limit", description = "The maximum number of items to return per page", isRequired = false, type = RestParameter.Type.INTEGER),
          @RestParameter(name = "offset", description = "The offset", isRequired = false, type = RestParameter.Type.INTEGER),
          @RestParameter(name = "name", isRequired = false, description = "Filter results by service name", type = STRING),
          @RestParameter(name = "host", isRequired = false, description = "Filter results by host name", type = STRING),
          @RestParameter(name = "q", isRequired = false, description = "Filter results by free text query", type = STRING),
          @RestParameter(name = "sort", isRequired = false, description = "The sort order.  May include any "
                  + "of the following: host, name, running, queued, completed,  meanRunTime, meanQueueTime, "
                  + "status. The sort suffix must be :asc for ascending sort order and :desc for descending.", type = STRING)
  }, reponses = { @RestResponse(description = "Returns the list of services from Opencast", responseCode = HttpServletResponse.SC_OK) }, returnDescription = "The list of services")
  public Response getJobs(@QueryParam("limit") final int limit, @QueryParam("offset") final int offset,
          @QueryParam("name") String name, @QueryParam("host") String host, @QueryParam("q") String text,
          @QueryParam("sort") String sort, @Context HttpHeaders headers) throws Exception {

    Option<String> nameOpt = Option.option(StringUtils.trimToNull(name));
    Option<String> hostOpt = Option.option(StringUtils.trimToNull(host));
    Option<String> textOpt = Option.option(StringUtils.trimToNull(text));
    Option<String> sortOpt = Option.option(StringUtils.trimToNull(sort));

    List<Service> services = new ArrayList<Service>();
    for (ServiceStatistics stats : serviceRegistry.getServiceStatistics()) {
      Service service = new Service(stats);
      if (nameOpt.isSome() && !StringUtils.equalsIgnoreCase(service.getName(), nameOpt.get())) {
        continue;
      }

      if (hostOpt.isSome() && !StringUtils.equalsIgnoreCase(service.getHost(), hostOpt.get())) {
        continue;
      }

      if (textOpt.isSome()) {
        if (!StringUtils.containsIgnoreCase(service.getName(), textOpt.get())
                && !StringUtils.containsIgnoreCase(service.getHost(), textOpt.get())) {
          continue;
        }
      }
      services.add(service);
    }
    int total = services.size();

    if (sortOpt.isSome()) {
      Set<SortCriterion> sortCriteria = RestUtils.parseSortQueryParameter(sortOpt.get());
      if (!sortCriteria.isEmpty()) {
        try {
          SortCriterion sortCriterion = sortCriteria.iterator().next();
          Collections.sort(services, new ServiceStatisticsComparator(
                  sortCriterion.getFieldName(),
                  sortCriterion.getOrder() == SearchQuery.Order.Ascending));
        } catch (Exception ex) {
          logger.warn("Failed to sort services collection.", ex);
        }
      }
    }

    List<JValue> jsonList = new ArrayList<JValue>();
    for (Service s : new SmartIterator<Service>(limit, offset).applyLimitAndOffset(services)) {
      jsonList.add(s.toJSON());
    }
    return RestUtils.okJsonList(jsonList, offset, limit, total);
  }

  /**
   * Service UI model. Wrapper class for a {@code ServiceStatistics} class.
   */
  class Service implements JSONAware {
    /** Completed model field name. */
    public static final String COMPLETED_NAME = "completed";
    /** Host model field name. */
    public static final String HOST_NAME = "host";
    /** MeanQueueTime model field name. */
    public static final String MEAN_QUEUE_TIME_NAME = "meanQueueTime";
    /** MeanRunTime model field name. */
    public static final String MEAN_RUN_TIME_NAME = "meanRunTime";
    /** (Service-) Name model field name. */
    public static final String NAME_NAME = "name";
    /** Queued model field name. */
    public static final String QUEUED_NAME = "queued";
    /** Running model field name. */
    public static final String RUNNING_NAME = "running";
    /** Status model field name. */
    public static final String STATUS_NAME = "status";

    /** Wrapped {@code ServiceStatistics} instance. */
    private final ServiceStatistics serviceStatistics;

    /** Constructor, set {@code ServiceStatistics} instance to a final private property. */
    Service(ServiceStatistics serviceStatistics) {
      this.serviceStatistics = serviceStatistics;
    }

    /**
     * Returns completed jobs count.
     * @return completed jobs count
     */
    public int getCompletedJobs() {
      return serviceStatistics.getFinishedJobs();
    }

    /**
     * Returns service host name.
     * @return service host name
     */
    public String getHost() {
      return serviceStatistics.getServiceRegistration().getHost();
    }

    /**
     * Returns service mean queue time.
     * @return service mean queue time
     */
    public long getMeanQueueTime() {
      return serviceStatistics.getMeanQueueTime();
    }

    /**
     * Returns service mean run time.
     * @return service mean run time
     */
    public long getMeanRunTime() {
      return serviceStatistics.getMeanRunTime();
    }

    /**
     * Returns service name.
     * @return service name
     */
    public String getName() {
      return serviceStatistics.getServiceRegistration().getServiceType();
    }

    /**
     * Returns queued jobs count.
     * @return queued jobs count
     */
    public int getQueuedJobs() {
      return serviceStatistics.getQueuedJobs();
    }

    /**
     * Returns running jobs count.
     * @return running jobs count
     */
    public int getRunningJobs() {
      return serviceStatistics.getRunningJobs();
    }

    /**
     * Returns service status.
     * @return service status
     */
    public ServiceState getStatus() {
      return serviceStatistics.getServiceRegistration().getServiceState();
    }

    /**
     * Returns a map of all service fields.
     * @return a map of all service fields
     */
    public Map<String, String> toMap() {
      Map<String, String> serviceMap = new HashMap<String, String>();
      serviceMap.put(COMPLETED_NAME, Integer.toString(getCompletedJobs()));
      serviceMap.put(HOST_NAME, getHost());
      serviceMap.put(MEAN_QUEUE_TIME_NAME, Long.toString(getMeanQueueTime()));
      serviceMap.put(MEAN_RUN_TIME_NAME, Long.toString(getMeanRunTime()));
      serviceMap.put(NAME_NAME, getName());
      serviceMap.put(QUEUED_NAME, Integer.toString(getQueuedJobs()));
      serviceMap.put(RUNNING_NAME, Integer.toString(getRunningJobs()));
      serviceMap.put(STATUS_NAME, getStatus().name());
      return serviceMap;
    }

    /**
     * Returns a json representation of a service as {@code String}.
     * @return a json representation of a service as {@code String}
     */
    @Override
    public String toJSONString() {
      return JSONObject.toJSONString(toMap());
    }

    /**
     * Returns a json representation of a service as {@code JValue}.
     * @return a json representation of a service as {@code JValue}
     */
    public JValue toJSON() {
      return j(f(COMPLETED_NAME, v(getCompletedJobs())), f(HOST_NAME, vN(getHost())),
              f(MEAN_QUEUE_TIME_NAME, v(getMeanQueueTime())), f(MEAN_RUN_TIME_NAME, v(getMeanRunTime())),
              f(NAME_NAME, vN(getName())), f(QUEUED_NAME, v(getQueuedJobs())),
              f(RUNNING_NAME, v(getRunningJobs())),
              f(STATUS_NAME, vN(getStatus().name())));
    }
  }

  /**
   * {@code Service} comparator. Can compare service instances based on the given sort criterion and sort order.
   */
  class ServiceStatisticsComparator implements Comparator<Service> {

    /** Sort criterion. */
    private final String sortBy;
    /** Sort order (true if ascending, false otherwise). */
    private final boolean ascending;

    /** Constructor. */
    ServiceStatisticsComparator(String sortBy, boolean ascending) {
      if (StringUtils.equalsIgnoreCase(Service.COMPLETED_NAME, sortBy)) {
        this.sortBy = Service.COMPLETED_NAME;
      } else if (StringUtils.equalsIgnoreCase(Service.HOST_NAME, sortBy)) {
        this.sortBy = Service.HOST_NAME;
      } else if (StringUtils.equalsIgnoreCase(Service.MEAN_QUEUE_TIME_NAME, sortBy)) {
        this.sortBy = Service.MEAN_QUEUE_TIME_NAME;
      } else if (StringUtils.equalsIgnoreCase(Service.MEAN_RUN_TIME_NAME, sortBy)) {
        this.sortBy = Service.MEAN_RUN_TIME_NAME;
      } else if (StringUtils.equalsIgnoreCase(Service.NAME_NAME, sortBy)) {
        this.sortBy = Service.NAME_NAME;
      } else if (StringUtils.equalsIgnoreCase(Service.QUEUED_NAME, sortBy)) {
        this.sortBy = Service.QUEUED_NAME;
      } else if (StringUtils.equalsIgnoreCase(Service.RUNNING_NAME, sortBy)) {
        this.sortBy = Service.RUNNING_NAME;
      } else if (StringUtils.equalsIgnoreCase(Service.STATUS_NAME, sortBy)) {
        this.sortBy = Service.STATUS_NAME;
      } else {
        throw new IllegalArgumentException(String.format("Can't sort services by %s.", sortBy));
      }
      this.ascending = ascending;
    }

    /**
     * Compare two service instances.
     * @param s1 first {@code Service} instance to compare
     * @param s2 second {@code Service} instance to compare
     * @return
     */
    @Override
    public int compare(Service s1, Service s2) {
      int result = 0;
      switch (sortBy) {
        case Service.COMPLETED_NAME:
          result = s1.getCompletedJobs() - s2.getCompletedJobs();
          break;
        case Service.HOST_NAME:
          result = s1.getHost().compareToIgnoreCase(s2.getHost());
          break;
        case Service.MEAN_QUEUE_TIME_NAME:
          result = (int) (s1.getMeanQueueTime() - s2.getMeanQueueTime());
          break;
        case Service.MEAN_RUN_TIME_NAME:
          result = (int) (s1.getMeanRunTime() - s2.getMeanRunTime());
          break;
        case Service.QUEUED_NAME:
          result = s1.getQueuedJobs() - s2.getQueuedJobs();
          break;
        case Service.RUNNING_NAME:
          result = s1.getRunningJobs() - s2.getRunningJobs();
          break;
        case Service.STATUS_NAME:
          result = s1.getStatus().compareTo(s2.getStatus());
          break;
        case Service.NAME_NAME: // default sorting criterium
        default:
          result = s1.getName().compareToIgnoreCase(s2.getName());
      }
      return ascending ? result : 0 - result;
    }
  }

  /** OSGI activate method. */
  public void activate() {
    logger.info("ServicesEndpoint is activated!");
  }

  /**
   * @param serviceRegistry
   *          the serviceRegistry to set
   */
  public void setServiceRegistry(ServiceRegistry serviceRegistry) {
    this.serviceRegistry = serviceRegistry;
  }
}
